# Company: giordano.ch AG
# Copyright by: giordano.ch AG
# https://giordano.ch/

#################################################################################
{
  "name"                 :  "SEO-URL Redirect/Rewrite",
  "summary"              :  """SEO-URL Redirect/Rewrite module helps to redirect or redirect a URL to another URL to avoid page not found error.""",
  "category"             :  "Website",
  "version"              :  "15.0.0.0",
  "sequence"             :  1,
  "author"               :  'giordano.ch AG',
  "website"              :  'https://www.giordano.ch',
  "description"          :  """SEO
                            Search Engine Optimization
                            URL
                            SEO URL
                            Redirect/Rewrite
                            Rewrite
                            Redirect
                            SEO-URL Redirect/Rewrite
                            Odoo SEO-URL Redirect/Rewrite
                            URL Redirect/Rewrite
                            URL Rewrite
                            URL Redirect""",
  # "live_test_url"        :  "http://odoodemo.webkul.com/?module=seo_url_redirect",
  "depends"              :  [
                             'website_sale',
                             'website_webkul_addons',
                             'wk_wizard_messages',
                            ],
  "data"                 :  [
                             'views/templates.xml',
                             'views/product_template_views.xml',
                             'views/product_views.xml',
                             'views/rewrite_view.xml',
                             'data/data_seo.xml',
                             'data/seo_server_actions.xml',
                             'views/website_views.xml',
                             'views/rewrite_menu.xml',
                             'views/res_config_views.xml',
                             'views/webkul_addons_config_inherit_view.xml',
                            ],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
  "price"                :  45,
  "currency"             :  "USD",
  "pre_init_hook"        :  "pre_init_check",
  "post_init_hook"       :  "_update_seo_url",
}
