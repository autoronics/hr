
from . import models
from . import controller

from odoo import api, SUPERUSER_ID
from odoo.addons.http_routing.models.ir_http import slug

def pre_init_check(cr):
    from odoo.service import common
    from odoo.exceptions import Warning
    version_info = common.exp_version()
    server_serie =version_info.get('server_serie')
    if server_serie!='15.0':raise Warning('Module support Odoo series 15.0 found {}.'.format(server_serie))

def _update_seo_url(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    try:
        categoryObjs = env['product.public.category'].search([])
        env['website.rewrite'].setSeoUrlKey('pattern_category', categoryObjs)
        templateObjs = env['product.template'].search([])
        env['website.rewrite'].setSeoUrlKey('pattern_product', templateObjs)
    except Exception as e:
        pass
