# Company: giordano.ch AG
# Copyright by: giordano.ch AG
# https://giordano.ch/

{
    'name': "Project Task Unique Sequence Number",
    'version': '15.0.1.0.2',
    'author': 'giordano.ch AG',
    'support': 'support@giordano.ch',
    'website': 'https://www.giordano.ch',
    'price': 49.0,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'summary': """This module will generate task and issue unique number for your projects.""",
    'description': """This module will generate Task's and Issue Sequence's project wise Unique Number.It will be also generate Project Unique Number,
                        If user want to set prefix manually it was be there,
                        Task Unique sequence Number will generate by projects if there,
                        Project has not be selected it's generate Task's Unique Number,
                        Issue Unique sequence Number will generate by projects if there,
                        Project has not be selected it's generate issue's Unique Number.
                        project issue unique serial number
                        issue unique serial number
                        issue number
                        task number
                        task unique number
                        issue unique number
                        project number
                        project issue serial number
                        unique number project task and issue
                        project task 
                        project issue
                        task numbering
                        issue numbering
                        unique numbering of task
                        unique numbering of issue
                        document number issue
                        document number task
                        sequence task
                        sequence issue
                        sequence project task
                        sequence project issue
                        project task sequence
                        project issue sequence
                        issue sequence
                        task sequence""",
    'images': ['static/description/img1.jpg'],
    'category': 'Project',
    'depends': ['project'],
    'data': ['data/project_sequence.xml',
             'views/project_view.xml',
             'views/project_task_view.xml',
             ],
    'installable': True,
    'application': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
