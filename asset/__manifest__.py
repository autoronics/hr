# Company: giordano.ch AG
# Copyright by: giordano.ch AG
# https://giordano.ch/

{
    'name': 'Assets',
    'version': '15.0',
    'summary': 'Asset Management',
    'description': """
Managing Assets in Odoo.
===========================
Support following feature:
    * Location for Asset
    * Assign Asset to employee
    * Track warranty information
    * Custom states of Asset
    * States of Asset for different team: Finance, Warehouse, Manufacture, Maintenance and Accounting
    * Drag&Drop manage states of Asset
    * Asset Tags
    * Search by main fields
    """,
    'author': 'giordano.ch AG',
    'support': 'support@giordano.ch',
    'website': 'https://www.giordano.ch',
    'category': 'Industries',
    'sequence': 0,
    'depends': ['mail', 'web'],
    'demo': ['asset_demo.xml'],
    'data': [
        'security/asset_security.xml',
        'security/ir.model.access.csv',
        'asset_view.xml',
        'asset_data.xml',
        #'stock_data.xml',
        'views/asset.xml',
    ],
    'installable': True,
    'application': True,
}
