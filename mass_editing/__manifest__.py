# Company: giordano.ch AG
# Copyright by: giordano.ch AG
# https://giordano.ch/
{
    'name': 'Mass Editing',
    'version': '15.0.1.0.2',
    'summary': 'Mass Editing',
    'category': 'Tools',
    'license': 'GPL-3 or any later version',
    'author': 'giordano.ch AG',
    'support': 'support@giordano.ch',
    'website': 'https://www.giordano.ch',
    'assets': {
        'web.assets_backend':['mass_editing/static/src/js/mass_editing.js']
    },
    'uninstall_hook': 'uninstall_hook',
    'depends': ['base', 'web'],
    'data': [
        'security/ir.model.access.csv',
        'views/mass_editing_view.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
