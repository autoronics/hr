odoo.define('knownsystem_app.fieldtexthtml', function (require) {
    'use strict';

    var FieldTextHtml = require('web_editor.backend').FieldTextHtml;
    var AbstractAction = require('web.AbstractAction');
    var core = require('web.core');
    var ajax = require('web.ajax');
    var FormController = require('web.FormController');
    var FormRenderer = require('web.FormRenderer');
    var FormView = require('web.FormView');
    var QWeb = core.qweb;
    var _t = core._t;

    FormRenderer.include({
        _addOnClickMapAction: function ($el, node) {
            var self = this;
            $el.click(function () {
                self.trigger_up('is_favourite', {
                    attrs: node.attrs,
                    record: self.state,
                });
            });
        },
        _renderHeaderButton: function (node) {
            var $button = this._super.apply(this, arguments);
            this._addOnClickMapAction($button, node);
            return $button;
        },

        _renderStatButton: function (node) {
            var $button = this._super.apply(this, arguments);
            this._addOnClickMapAction($button, node);
            return $button;
        },
    });

    FormController.include({

        custom_events: _.extend({}, FormController.prototype.custom_events, {
            is_favourite: '_onClickIsFavButton',
        }),

        className: 'oe_star_button',

        init: function () {
            this._super.apply(this, arguments);
        },

        _onClickIsFavButton: function (event) {
            var self = this;
            if(event.data.record){
                if(event.data.record.model == 'knowledge.bases') {
                    if(event.data.record.data) {
                        if(event.data.record.data.favourite_lines) {
                            var id = event.data.record.data.id;
                            var favourite_lines = event.data.record.data.favourite_lines;
                            var uid = event.data.record.context.uid;
                            var fav_list = favourite_lines.data
                            for(var i=0;i<fav_list.length;i++){
                                if(uid === fav_list[i].data.user_id.res_id)
                                {
                                    if(fav_list[i].data.is_fav === true){
                                    }
                                    else{
                                    }
                                }
                            }
                            
                        } 
                    }
                }        
            }
        },
    });


    FieldTextHtml.include({
        getDatarecord: function () {
            var datarecord = this._super();
            if (this.model === 'knowledge.bases') {
                datarecord = _.omit(datarecord, function (val, key) {
                    return _.isObject(val) || key === 'kms_snippets';
                });
            }
            return datarecord;
        },
    });
});
