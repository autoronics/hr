odoo.define('knownsystem_app.portal_kms', function (require) {
'use strict';

    var core = require('web.core');
    var ajax = require('web.ajax');
    var rpc = require('web.rpc');
    var request
    var _t = core._t;
    $(document).ready(function(){
        $('.o_dislike').click(function(ev) {
            var $input = $(ev.currentTarget);
            var record_id = $input.context.previousElementSibling.defaultValue;
            rpc.query({
                model: 'knowledge.bases',
                method: 'dislike_recods',
                args: [1,record_id],
            }).then(function() {
                location.reload()
            });
        });

        $('.o_like').click(function(ev) {
            var $input = $(ev.currentTarget);
            var record_id = $input.context.previousElementSibling.defaultValue;
            rpc.query({
                model: 'knowledge.bases',
                method: 'like_recods',
                args: [1,record_id],
            }).then(function() {
                location.reload();
            });
        });
    })
});