odoo.define('knownsystem_app.kanbanModelInclude', function(require) {
    "use strict";
    var KanbanModel = require('web.KanbanModel');
    var core = require('web.core');
    var _t = core._t;
    KanbanModel.include({
        reload: function(id, options) {
            options = options || {};
            var element = this.localData[id];
            var searchDomain = options.domain || element.searchDomain || [];
            element.searchDomain = options.searchDomain = searchDomain;
            if (options.KmsDomain !== undefined) {
                options.domain = searchDomain.concat(options.KmsDomain);
            };
            return this._super.apply(this, arguments)
        },
    });
});;




odoo.define('knownsystem_app.kanbanRecordInclude', function(require) {
    "use strict";
    var KanbanRecord = require('web.KanbanRecord');

    KanbanRecord.include({
        events: _.extend({}, KanbanRecord.prototype.events, {
            'click .select_kms': '_KmsSelection',
            // 'click .my_fav_record' : 'MyFavRecord',
        }),

        _updateSelect: function(event, selected) {
            event.stopImmediatePropagation();
            this.trigger_up('selected_kms', {
                originalEvent: event,
                resID: this.id,
                selected: selected,
            });

            // this.trigger_up('my_fav_record', {
            //     originalEvent: event,
            //     resID: this.id,
            //     fav_kms: selected,
            // });
        },
        _updateRecordView: function(select) {
            var kanbanCard = this.$el;
            var checkBox = this.$el.find(".select_kms");
            if (select) {
                checkBox.removeClass("fa-square-o");
                checkBox.addClass("fa-check-square-o green");
                kanbanCard.addClass("selected_record");
            } else {
                checkBox.removeClass("fa-check-square-o green");
                checkBox.addClass("fa-square-o");
                kanbanCard.removeClass("selected_record");
            };
        },

        // _updateFav: function(select) {
        //     var kanbanCard = this.$el;
        //     var checkBox = this.$el.find(".select_kms");
        //     if (select) {
        //         checkBox.removeClass("fa-heart-o");
        //         checkBox.addClass("fa-heart");
        //         kanbanCard.addClass("fav_record");
        //     } else {
        //         checkBox.removeClass("fa-heart");
        //         checkBox.addClass("fa-heart-o");
        //         kanbanCard.removeClass("fav_record");
        //     };
        // },

        _KmsSelection: function(event) {
            event.stopImmediatePropagation();
            var checkBox = this.$el.find(".select_kms");
            if (checkBox.hasClass("fa-square-o")) {
                this._updateRecordView(true)
                this._updateSelect(event, true);
            } else {
                this._updateRecordView(false);
                this._updateSelect(event, false);
            }
        },

        // MyFavRecord: function(event){
        //     var self = this;
        //     event.stopImmediatePropagation();
        //     var heart = this.$el.find(".my_fav_record");
        //     if (heart.hasClass("fa-heart-o")) {
        //         this._updateFav(true)
        //         this._updateSelect(event, true);
        //     } else {
        //         this._updateFav(false);
        //         this._updateSelect(event, false);
        //     }
        // },
    });
});;


odoo.define('knownsystem_app.kanbanRendererInclude', function(require) {
    "use strict";
    var KanbanRenderer = require('web.KanbanRenderer');
    KanbanRenderer.include({

        updateSelection: function(recordsPicked) {
            var self = this;
            _.each(this.widgets, function(widget) {
                var selected = _.contains(recordsPicked, widget.id);
                widget._updateRecordView(selected);
            });
        },
    });
});;


odoo.define('knownsystem_app.ControlPanelInclude', function(require) {
    "use strict";
    var ControlPanel = require('web.ControlPanel');
    ControlPanel.include({
        init: function(parent, template) {
            this._super(parent);
        },

        _update_search_view: function (searchview, isHidden, groupable, enableTimeRangeMenu) {
            if (searchview) {
                searchview.toggle_visibility(!isHidden);
                if (groupable !== undefined){
                    if(searchview.title === "knowledge Base"){
                        searchview.groupby_menu.do_toggle(false);
                    }
                    else{
                        searchview.groupby_menu.do_toggle(groupable);
                    }
                }
                if (enableTimeRangeMenu !== undefined){
                    searchview.displayTimeRangeMenu(enableTimeRangeMenu);
                }
            }
            this.nodes.$searchview.toggle(!isHidden);
            this.$el.toggleClass('o_breadcrumb_full', !!isHidden);
        },
    });
});;





odoo.define('knownsystem_app.kanbanControllerInclude', function (require) {
    'use strict';

    var Context = require('web.Context');
    var core = require('web.core');
    var Domain = require('web.Domain');
    var view_dialogs = require('web.view_dialogs');
    var viewUtils = require('web.viewUtils');
    var KanbanController = require('web.KanbanController');

    var _t = core._t;
    var qweb = core.qweb;

    KanbanController.include({
        events: _.extend({}, KanbanController.prototype.events, {            
            'click .kms-select-record': 'SelectKmsRecords',
            'change .kms-input': '_KmsRecordSorting',
            'click .kms-remove-selected': 'RemovePickedRecords',
            'click .like-selected' : 'LikeSelectedKms',
            'click .dislike-selected' : 'DisLikeSelectedKms',
            'click .publish-selected' : 'PublishSelectedKms',
            'click .duplicate-selected' : 'DuplicateSelectedKms',
            'click .print-selected' : 'PrintSelectedKms',
            'click .archive-selected' : 'ArchiveSelectedKms',
            'click .update-selected' : 'UpdateSelectedKms',
        }),

        custom_events: _.extend({}, KanbanController.prototype.custom_events, {
            selected_kms: '_selectedKMS',
            // fav_kms: '_favKMS',
        }),

        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);
            this.kanban_update = false;
            this.recordsPicked = [];
            this.recordsFav = [];
        },

        start: function() {
            var self = this;
            if(self.modelName === 'knowledge.bases'){
                this.$el.addClass('main-kms d-flex');
                return this._super.apply(this, arguments);
            }
            else{
                return this._super.apply(this, arguments);   
            }
        },

        update: function(params, options) {
            var self = this;
            if(self.modelName === 'knowledge.bases'){
                var domain = params.domain || []
                this.nonavigation_update = true;
                params.KmsDomain = this._renderArticles();
                return this._super(params, options); 
            }
            else{
                return this._super(params, options); 
            }
        },

        _update: function() {
            var self = this;
            return this._super.apply(this, arguments).then(function() {
                var state = self.model.get(self.handle);
                if (state.model === 'knowledge.bases') {
                    self._renderLeftPanel();
                    self.renderer.updateSelection(self.recordsPicked);
                };
            });
        },

        _renderLeftPanel: function() {
            var self = this;

            var scrollTop = self.$('.kms-left-panel').scrollTop();
            self.$('.kms-left-panel').remove();
            var navigationElements = {
                'right_for_create': self.is_action_enabled('create'),
                'right_for_delete': self.is_action_enabled('delete'),
            };
            var $navigationPanel = $(qweb.render('KmsLeftPanel', navigationElements));
            self.$el.prepend($navigationPanel);
            var defer = self._renderSections();
            $.when(defer).then(function() {
                self._renderTags();
            });
            // self._renderTypes();
            self.$('.kms-left-panel').scrollTop(scrollTop || 0);
        },

        _renderSections: function() {
            var self = this;
            var defer = $.Deferred();
            if(self.modelName === 'knowledge.bases'){
                self.$('#sections').jstree('destroy');
                var sections = $.get('/kms/sections', function(data) {
                    var Jdata = JSON.parse(data)
                    var TreeData = {
                        'core': {
                            'themes': {
                                'icons': false
                            },
                            'check_callback': true,
                            'data': Jdata,
                            "multiple": true,
                        },
                        "state": {
                            "key": "sections"
                        },
                        "checkbox": {
                            "three_state": false,
                            "cascade": "down",
                            "tie_selection": false,
                        },
                        "plugins": ["contextmenu", "checkbox", "dnd", "state", "search"],
                    }

                    var ref = self.$('#sections').jstree(TreeData);

                    self.$('#sections').on("state_ready.jstree", self, function(event, data) {
                        self.$('#sections').on("check_node.jstree uncheck_node.jstree", self, function(event, data) {
                            self.reload();
                        })
                    });

                    defer.resolve();
                });
                return defer
            }
            else{
                defer.resolve();
                return defer
            }
        },

        // _renderTypes: function() {
        //     var self = this;
            
        //     var defer = $.Deferred();
        //     if(self.modelName === 'knowledge.bases'){
        //         self.$('#types').jstree('destroy');
        //         var types = $.get('/kms/types', function(data) {
        //             var Jdata = JSON.parse(data)
        //             var TreeData = {
        //                 'core': {
        //                     'themes': {
        //                         'icons': false
        //                     },
        //                     'check_callback': true,
        //                     'data': Jdata,
        //                     "multiple": true,
        //                 },
        //                 "plugins": ["contextmenu", "checkbox", "state", "search", ],
        //                 "state": {
        //                     "key": "types"
        //                 },
        //                 "checkbox": {
        //                     "three_state": false,
        //                     "cascade": "down",
        //                     "tie_selection": false,
        //                 },
        //             }
        //             var ref = self.$('#types').jstree(TreeData);


        //             self.$('#types').on("state_ready.jstree", self, function(event, data) {
        //                 self.$('#types').on("check_node.jstree uncheck_node.jstree", self, function(event, data) {
        //                     self.reload();
        //                 })
        //             });

        //             defer.resolve();
        //         });
        //         return defer
        //     }
        //     else{
        //         defer.resolve();
        //         return defer
        //     }
        // },

        _renderTags: function(defer) {
            var self = this;
            self.$('#tags').jstree('destroy');
            var tags = $.get('/kms/tags', function(data) {
                var Jdata = JSON.parse(data)
                var TreeData = {
                    'core': {
                        'themes': {
                            'icons': false
                        },
                        'check_callback': true,
                        'data': Jdata,
                        "multiple": true,
                    },
                    "plugins": ["contextmenu", "checkbox", "state", "search", ],
                    "state": {
                        "key": "tags"
                    },
                    "checkbox": {
                        "three_state": false,
                        "cascade": "down",
                        "tie_selection": false,
                    },
                }
                var ref = self.$('#tags').jstree(TreeData);


                self.$('#tags').on("state_ready.jstree", self, function(event, data) {
                    self.$('#tags').on("check_node.jstree uncheck_node.jstree", self, function(event, data) {
                        self.reload();
                    });
                });
            });
        },

        _renderArticles: function() {
            var self = this;
            var domain = [];
            var refS = self.$('#sections').jstree(true)
            var selectedSections = refS.get_checked()
            var selectedSectionsIDS = selectedSections.map(function(item) {
                    return parseInt(item, 10);
                });
            if (selectedSectionsIDS.length != 0) {
                domain.push(['section_id', 'in', selectedSectionsIDS]);
            }
            var refT = self.$('#tags').jstree(true)            
            var selectedTags = refT.get_checked()
            var tagsLength = selectedTags.length
            if (tagsLength != 0) {
                var iterator = 0;
                while (iterator != tagsLength - 1) {
                    domain.push('|');
                    iterator++;
                }
                _.each(selectedTags, function(tag) {
                    if (tag.length) {
                        domain.push(['tag_ids', 'in', parseInt(tag)]);
                    }
                });
            };
            return domain
        },

        _selectedKMS: function(event) {
            event.stopPropagation();
            var eventData = event.data;
            var addToSelection = eventData.selected;
            if (addToSelection) {
                this.recordsPicked.push(eventData.resID);
            } else {
                this.recordsPicked = _.without(this.recordsPicked, eventData.resID);
            };
            this._renderRightPanel();
        },

        // _favKMS: function(event) {
        //     event.stopPropagation();
        //     var eventData = event.data;
        //     var addToFav = eventData.fav_kms;
        //     if (addToFav) {
        //         this.recordsFav.push(eventData.resID);
        //     } else {
        //         this.recordsFav = _.without(this.recordsPicked, eventData.resID);
        //     };
        // },

        PublishSelectedKms: function(event){
            event.stopImmediatePropagation();
            var self = this;
            var alreadySelected = self.recordsPicked;
            this._rpc({
                model: 'knowledge.bases',
                method: 'publish_recods',
                args: [1,alreadySelected],
            }).then(function() {
                self.recordsPicked = [];
                self.renderer.updateSelection(self.recordsPicked);
                self._renderRightPanel();
                self.reload();
            });
        }, 

        PrintSelectedKms: function(event){
            event.stopImmediatePropagation();
            var self = this;
            var alreadySelected = self.recordsPicked;
            self.do_action('knownsystem_app.kms_report_id',{
                additional_context:{active_ids:alreadySelected,}})
            self.recordsPicked = [];
            self.renderer.updateSelection(self.recordsPicked);
            self._renderRightPanel();
            self.reload();
        },  

        ArchiveSelectedKms: function(event){
            event.stopImmediatePropagation();
            var self = this;
            var alreadySelected = self.recordsPicked;
            this._rpc({
                model: 'knowledge.bases',
                method: 'archive_recods',
                args: [1,alreadySelected],
            }).then(function() {
                self.recordsPicked = [];
                self.renderer.updateSelection(self.recordsPicked);
                self._renderRightPanel();
                self.reload();
            });
            
        },

        UpdateSelectedKms: function(event){
            event.stopImmediatePropagation();
            var self = this;
            var alreadySelected = self.recordsPicked;
            var context = {
                active_model: self.modelName,
                active_ids : alreadySelected,
            }
            var action = {
                type: 'ir.actions.act_window',
                res_model: 'kms.mass.update',
                view_mode: 'form',
                view_type: 'form',
                views: [[false, 'form']],
                target: 'new',
                context:context,
            };
            self.do_action(action)
        }, 

        DuplicateSelectedKms: function(event){
            event.stopImmediatePropagation();
            var self = this;
            var alreadySelected = self.recordsPicked;
            this._rpc({
                model: 'knowledge.bases',
                method: 'duplicate_recods',
                args: [1,alreadySelected],
            }).then(function() {
                self.reload();
            });
        },

        DisLikeSelectedKms: function(event){
            event.stopImmediatePropagation();
            var self = this;
            var alreadySelected = self.recordsPicked;
            this._rpc({
                model: 'knowledge.bases',
                method: 'dislike_recods',
                args: [1,alreadySelected],
            }).then(function() {
                self.reload();
            });
        },

        LikeSelectedKms: function(event){
            event.stopImmediatePropagation();
            var self = this;
            var def;
            var alreadySelected = self.recordsPicked;
            this._rpc({
                model: 'knowledge.bases',
                method: 'like_recods',
                args: [1,alreadySelected],
            }).then(function() {
                self.reload();
            });
        },


        SelectKmsRecords: function(event){
            event.stopImmediatePropagation();
            var self = this;
            var alreadySelected = self.recordsPicked;
            var data = self.model.get(this.handle);
            var list = self.model.localData[data.id];
            var resIDS = list.res_ids;
            self.recordsPicked = resIDS;
            self.renderer.updateSelection(resIDS);
            self._renderRightPanel();
        },

        RemovePickedRecords: function(event) {
            event.stopImmediatePropagation();
            var self = this;
            self.recordsPicked = [];
            self.renderer.updateSelection(self.recordsPicked);
            self._renderRightPanel();
        },

        _renderRightPanel: function() {
            var self = this
            var scrollTop = self.$('.kms-right-panel').scrollTop();
            self.$('.kms-right-panel').remove();
            $('.o_kanban_view').removeClass('o_kms_kanban_rl');
            $('.o_kanban_view').addClass('o_kms_kanban');
            var recordsPicked = this.recordsPicked;
            if (recordsPicked.length) {
                var data = {'articles':JSON.stringify(recordsPicked)}
                var articles = $.ajax({
                    url: "/kms/articles",
                    method: "GET",
                    dataType: 'json',
                    data : data,
                    success: function(data) {
                        $('.o_kanban_view').addClass('o_kms_kanban_rl');
                        $('.o_kanban_view').removeClass('o_kms_kanban');
                        var navigationElements = {
                            'counts' : data[0].counts,
                            'articles' : data[0].articles,
                            'names' : data[0].names,
                        };
                        var $navigationPanel = $(qweb.render('KmsRightPanel', navigationElements));
                        self.$el.prepend($navigationPanel);

                        $('.kms-remove-selected').on('mouseenter',function(){
                                var title = $(this).attr('title');
                                $(this).data('tipText', title).removeAttr('title');
                                $('<p class="tooltip"></p>')
                                .text(title)
                                .appendTo('body')
                                .fadeIn('slow');
                            }, function() {
                                $(this).attr('title', $(this).data('tipText'));
                                $('.tooltip').remove();
                            }).mousemove(function(e) {
                                var mousex = e.pageX + 20; //Get X coordinates
                                var mousey = e.pageY + 10; //Get Y coordinates
                                $('.tooltip')
                                .css({ top: mousey, left: mousex })                            
                        });
                    }
                });
            }
            else{
                $('.o_kanban_view').removeClass('o_kms_kanban_rl');
                $('.o_kanban_view').addClass('o_kms_kanban');
            }
        },

        _KmsRecordSorting: function(event, passed){
            event.stopPropagation();
            event.preventDefault();
            var self = this;
            var sortKey = event.currentTarget.value;
            var data = this.model.get(this.handle);
            var list = this.model.localData[data.id];
            var asc = true;
            if (passed && passed.reverse) {
                if (list.orderedBy.length != 0 && list.orderedBy[0].name == sortKey) {
                    asc = list.orderedBy[0].asc;
                } else {
                    asc = false;
                };
            };
            list.orderedBy = [];
            list.orderedBy.push({
                name: sortKey,
                asc: asc
            });
            this.model.setSort(data.id, sortKey).then(function() {
                self.reload({});
            });
        }

    });
});;
