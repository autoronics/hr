odoo.define('knownsystem_app.FormControllerInclude', function (require) {
"use strict";

    var AbstractAction = require('web.AbstractAction');
    var core = require('web.core');
    var ajax = require('web.ajax');
    var FormController = require('web.FormController');
    var FormRenderer = require('web.FormRenderer');
    var FormView = require('web.FormView');
    var QWeb = core.qweb;
    var _t = core._t;


    FormController.include({

            custom_events: _.extend({}, FormController.prototype.custom_events, {
                fav_kms: '_onFavKms',
            }),

            className: 'oe_fav_button',

            init: function () {
                this._super.apply(this, arguments);
            },

            renderButtons: function($node) {
                var self = this;
                $.when(this._super.apply(this, arguments)).then(function() {
                    if (self.modelName === "knowledge.bases") {
                        self.$buttons.on('click', '.o_form_button_fav', self._onKmsFavourite.bind(self));
                    };
                });
            },

            _onKmsFavourite: function(event) {
                var self = this;
                var kms = this.model.localData[this.handle],
                    kmsID = kms.data.id;
                this._rpc({
                    model: "knowledge.bases",
                    method: 'favourite_kms',
                    args: [
                        [kmsID]
                    ],
                }).then(function(data) {
                    self._updateFavourite(data);
                });
            },

            _updateFavourite: function(data) {
                var self = this;
                var new_data = JSON.parse(data);
                if (new_data.is_fav) {
                    this.$buttons.find('#fav_kms_button').removeClass("fa-star-o");
                    this.$buttons.find('#fav_kms_button').addClass("fa-star");
                } else {
                    this.$buttons.find('#fav_kms_button').removeClass("fa-star");
                    this.$buttons.find('#fav_kms_button').addClass("fa-star-o");
                };
            },


    });


});