odoo.define('knownsystem_app.Service', function (require) {
"use strict";

var core = require('web.core');

var KnowledgeManager = require('knownsystem_app.Manager');

require('mail.Manager.Notification');
require('mail.Manager.Window');
require('mail.Manager.DocumentThread');

core.serviceRegistry.add('knowledge_service', KnowledgeManager);

return KnowledgeManager;

});
