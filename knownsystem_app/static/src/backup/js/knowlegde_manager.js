odoo.define('knownsystem_app.Manager', function (require) {
"use strict";

/**
 * This service is responsible for anything related to mail in JS.
 * In particular, it stores all threads and messages that have been fetched
 * from the server.
 *
 * As this service does quite a lot of stuffs, it is split into many different
 * files. For instance, the handling of notification in the longpoll is in
 * mail_notification_manager.js. This file contains the core functionnality
 * of the mail service.
 *
 * The main point of entry of this service is to call public methods by mean
 * of a service call.
 *
 * @example
 *
 * To get all threads that have been fetched from the server:
 *
 *      this.call('mail_service', 'getKmThreads');
 *
 * To get a particular thread with server ID 1:
 *
 *      this.call('mail_service', 'getKmThread', 1);
 */
var AbstractService = require('web.AbstractService');

var DMChat = require('mail.model.DMChat');
var Livechat = require('mail.model.Livechat');
var Mailbox = require('mail.model.Mailbox');
var MailFailure = require('mail.model.MailFailure');
var Message = require('mail.model.Message');
var MultiUserChannel = require('mail.model.MultiUserChannel');
var mailUtils = require('mail.utils');

var Bus = require('web.Bus');
var config = require('web.config');
var core = require('web.core');
var session = require('web.session');

var _t = core._t;

var PREVIEW_MSG_MAX_SIZE = 350;  // optimal for native english speakers

var KnowledgeManager =  AbstractService.extend({
    dependencies: ['ajax', 'bus_service', 'local_storage'],
    _ODOOBOT_ID: "ODOOBOT", // default authorID for transient messages
	

	start: function () {
        this._super.apply(this, arguments);
        this._initializeKnowledgeState();
        this._fetchKnowledgeStateFromServer();
    },

    _initializeKnowledgeState: function () {
        this._kmsBus = new Bus(this);
        this._knowthreads = [];
        this._pinnedDmSections = [];
        this._mentionSectionSuggestions = [];
    },


    isReady: function () {
        return this._isReady;
    },

    getKmThread: function (threadID) {
        return _.find(this._knowthreads, function (thread) {
            return thread.getID() === threadID;
        });
    },
    /**
     * Returns a list of threads
     *
     * @returns {mail.model.Thread[]} list of threads
     */

    getKmThreads: function () {
        return this._knowthreads;
    },

    _fetchKnowledgeStateFromServer  : function () {
        var self = this;
        this._isReady = session.is_bound.then(function () {
            var context = _.extend(
                { isMobile: config.device.isMobile },
                session.user_context
            );
            return self._rpc({
                route: '/know/init_messaging',
                params: { context: context },
            });
        }).then(function (result) {

            self._updateKnowledgeStateFromServer (result);
            self.call('bus_service', 'startPolling');
        });
    },

    // getDiscussMenuID: function () {
    //     return this._discussMenuID;
    // },
    /**
     * Gets direct message section
     *
     * @param {integer} sectionID
     * @returns {Object|undefined} section
     */

    kms_redirect: function (resModel, resID, dmRedirectionCallback) {
        
        if (resModel === 'knowledge.section') {
            this._redirectSection(resModel, resID, dmRedirectionCallback);
        }
    },


    _redirectSection: function (resModel, resID, dmRedirection) {
        var self = this;
        var domain = [['id', '=', resID]];
        this._rpc({
                model: 'knowledge.section',
                method: 'search',
                args: [domain],
            })
            .then(function (userIDs) {
                self.createKnowledge(resID, 'kms_section').then(dmRedirection);
            });
    },

    createKnowledge: function (name, type) {
        if (type === 'kms_section') {
            return this._createSection(name);
        } else {
            return this._createSection(name, type);
        }
    },

    _createSection: function (name) {
        var context = _.extend({ isMobile: config.device.isMobile }, session.user_context);
        return this._rpc({
                model: 'knowledge.knowledge',
                method: 'knowledge_get',
                args: [[name]],
                kwargs: { context: context },
            })
            .then(this._addSection.bind(this));
    },

    getDMChatFromSectionID: function (sectionID) {
        // console.log(this._knowthreads,'---------_knowthreads---------------')
        // console.log(sectionID,'-------sectionID-----------------')
        return _.find(this._knowthreads, function (thread) {
            console.log(thread,thread.getType(),'-------thread-----------------')
            return thread.getType() === 'kms_section' &&
                    thread.getDirectSectionID() === sectionID;
        });
    },

    _searchSectionPrefetch: function (searchVal, limit) {
        var values = [];
        var searchRegexp = new RegExp(_.str.escapeRegExp(mailUtils.unaccent(searchVal)), 'i');
        _.each(this._mentionSectionSuggestions, function (sections) {
            if (values.length < limit) {
                values = values.concat(_.filter(sections, function (section) {
                    return (session.section_id !== section.id) &&
                            searchRegexp.test(section.name);
                })).splice(0, limit);
            }
        });
        return values;
    },

    searchSections: function (searchVal, limit) {
        var def = $.Deferred();
        var sections = this._searchSectionPrefetch(searchVal, limit);
        // console.log(sections,'===============================sections')
        if (!sections.length) {
            def = this._searchSectionFetch(searchVal, limit);
        } else {
            def = $.when(sections);
        }
        // console.log(def,'===============================def')
        return def.then(function (sections) {
            var suggestions = _.map(sections, function (section) {
                return {
                    id: section.id,
                    value: section.name,
                    label: section.name
                };
            });
            // console.log(suggestions,'===============================suggestions')
            return _.sortBy(suggestions, 'label');
        });
    },

    _searchSectionFetch: function (searchVal, limit) {
        return this._rpc({
                model: 'knowledge.section',
                method: 'im_search',
                args: [searchVal, limit || 20],
            }, { shadow: true });
    },

    _updatesectionsFromServer: function (data) {
        var self = this;
        _.each(data.section_slots, function (sections) {
            _.each(sections, self._addSection.bind(self));
        });
    },

    _addSection: function (data, options) {
        options = typeof options === 'object' ? options : {};

        var section = this.getSection(data.id);

        
        if (!section) {
            section = this._makeSection(data, options);
            if (section.getType() === 'kms_section') {
                this._pinnedDmSections.push(section.getDirectSectionID());
                this.call('bus_service', 'updateOption',
                    'section_id',
                    this._pinnedDmSections
                );
            }
            this._knowthreads.push(section);
            // if (data.last_message) {
            //     // section_info in mobile, necessary for showing section
            //     // preview in mobile
            //     this.addMessage(data.last_message);
            // }
            this._sortThreads();
            // if (!options.silent) {
            //     this._mailBus.trigger('new_section', section);
            // }
        }
        return section.getID();
    },

    getSection: function (threadID) {
        return _.find(this._knowthreads, function (thread) {
            return (thread.getID() === threadID);
        });
    },

    _makeSection: function (data, options) {
        
        if (data.section_type === 'kms_section') {
            return new KmsArticle({
                parent: this,
                data: data,
                options: options,
                commands: this._commands,
            });
        }
        
    },

    _updateKnowledgeStateFromServer : function (result) {
        
        this._updatesectionsFromServer(result);
        this._updateKnowledgeboxesFromServer(result);
        
        this._mentionSectionSuggestions = result.mention_section_suggestions;
        this._discussMenuID = result.menu_id;
    },

    _updateKnowledgeboxesFromServer: function (data) {
        this._addKnowbox({
            id: 'articles',
            name: _t("Section"),
            mailboxCounter: data.needaction_inbox_counter || 0,
        });
    },

    _addKnowbox: function (data, options) {
        options = typeof options === 'object' ? options : {};
        var mailbox = new Mailbox({
            parent: this,
            data: data,
            options: options,
            commands: this._commands
        });
        this._knowthreads.push(mailbox);
        this._sortThreads();
        return mailbox;
    },

    getMailBus: function () {
        return this._kmsBus;
    },
    
    _sortThreads: function () {
        this._knowthreads = _.sortBy(this._knowthreads, function (thread) {
            var name = thread.getName();
            return _.isString(name) ? name.toLowerCase() : '';
        });
    },

    _joinAndAddKMS: function (kmsID, options) {
        var self = this;
        return this._rpc({
                model: 'knowledge.knowledge',
                method: 'knowledge_get_info',
                args: [[kmsID]],
            })
            .then(function (result) {
                return self._addKMS(result, options);
            });
    },

    joinKMS: function (kmsID, options) {
        var def;
        var KMS = this.getKMS(kmsID);
        if (KMS) {
            // KMS already joined
            def = $.when(kmsID);
        } else {
            def = this._joinAndAddKMS(kmsID, options);
        }
        return def;
    },


   });

    return KnowledgeManager;
});