odoo.define('knownsystem_app.KnowledgeBase', function (require) {
"use strict";

var BasicComposer = require('mail.composer.Basic');
var ExtendedComposer = require('mail.composer.Extended');
var ThreadWidget = require('mail.widget.Thread');

var AbstractAction = require('web.AbstractAction');
var config = require('web.config');
var ControlPanelMixin = require('web.ControlPanelMixin');
var core = require('web.core');
var data = require('web.data');
var Dialog = require('web.Dialog');
var dom = require('web.dom');
var pyUtils = require('web.py_utils');
var SearchView = require('web.SearchView');
var session = require('web.session');

var QWeb = core.qweb;
var _t = core._t;

var KnowledgeBase = AbstractAction.extend(ControlPanelMixin, {
    template: 'knowledge.articles',

    custom_events: {
        message_moderation: '_onMessageModeration',
        search: '_onSearch',
    },
    events: {
        'click .o_kms_sidebar_title .o_add_kms': '_onAddKmsThread',
        'click .o_kms_channel_settings': '_onKmsSettingsClicked',
        
    },

    init: function (parent, action, options) {
        this._super.apply(this, arguments);
        this.action = action;
        this.dataset = new data.DataSetSearch(this, 'knowledge.knowledge');
        this.action_manager = parent;
        this.domain = [];
        this.options = options || {};

        this._threadsScrolltop = {};
        this._composerStates = {};
        this._defaultThreadID = this.options.active_id ||
                                this.action.context.active_id ||
                                this.action.params.default_active_id ||
                                'mailbox_articles';
        this._selectedMessage = null;
        this._throttledUpdateThreads = _.throttle(
            this._updateThreads.bind(this), 100, { leading: false });
    },

    willStart: function () {
        var self = this;
        var viewID = this.action &&
                        this.action.search_view_id &&
                        this.action.search_view_id[0];
        var def = this
            .loadFieldView(this.dataset, viewID, 'search')
            .then(function (fieldsView) {
                self.fields_view = fieldsView;
            });
        return $.when(this._super(), def);
    },

    start: function () {
        var self = this;
        this._basicComposer = new BasicComposer(this,
            { mentionPartnersRestricted: true }
        );
        this._extendedComposer = new ExtendedComposer(this,
            { mentionPartnersRestricted: true }
        );
        

        this._renderButtons();

        var defs = [];
        defs.push(
            this._renderThread()
        );
        
        defs.push(this._renderSearchView());

        return this.alive($.when.apply($, defs))
            .then(function () {
                return self.alive(self._setThread(self._defaultThreadID));
            })
            .then(function () {
                self._updateThreads();
                self._startListening();
            });
    },  


    _onKmsSettingsClicked: function (ev) {
        var threadID = $(ev.target).data('thread-id');
        this.do_action({
            type: 'ir.actions.act_window',
            res_model: 'knowledge.knowledge',
            res_id: threadID,
            views: [[false, 'form']],
            target: 'current'
        });
    },


    _renderSearchView: function () {
        var self = this;
        var options = {
            $buttons: $('<div>'),
            action: this.action,
            disable_groupby: true,
        };
        this.searchview = new SearchView(this, this.dataset, this.fields_view, options);
        return this.alive(this.searchview.appendTo($('<div>')))
            .then(function () {
                self.$searchview_buttons = self.searchview.$buttons;
                self.searchview.do_search();
            });
    },


    _startListening: function () {
        this.call('knowledge_service', 'getMailBus')
        .on('activity_updated', this, this._throttledUpdateThreads);
    },

    _renderThread: function () {
        this._threadWidget = new ThreadWidget(this, {
            loadMoreOnScroll: true
        });
        this._threadWidget
            .on('kms_redirect', this, function (resModel, resID) {
                this.call('knowledge_service', 'kms_redirect', resModel, resID, this._setThread.bind(this));
            })

        return this._threadWidget.appendTo(this.$('.o_kms_discuss_content'));
    },

    _renderButtons: function () {
        this.$buttons = $(QWeb.render('knowledge.articles.ControlButtons', { debug: session.debug }));
        this.$buttons.find('button').css({display:'inline-block'});
    },

    _onAddKmsThread: function (ev) {
        ev.preventDefault();
        var type = $(ev.target).data('type');
        this.$('.o_kms_add_thread[data-type=' + type + ']')
            .show()
            .find('input').focus();
    },

    _onMessageModeration: function (ev) {
    },

    _updateThreads: function () {
        var self = this;

        var $sidebar = this._renderSidebar({
            threads: _.filter(this.call('knowledge_service', 'getKmThreads'), function (thread) {
                return thread.getType() !== 'document_thread';
            }),
        });

        this.$('.o_kms_discuss_sidebar').html($sidebar.contents());
        _.each(['kms_section'], function (type) {
            var $input = self.$('.o_kms_add_thread[data-type=' + type + '] input');
            self._prepareAddThreadInput($input, type);
        });
    },

    _prepareAddThreadInput: function ($input, type) {
        var self = this;
        if (type === 'kms_section') {
            $input.autocomplete({
                source: function (request, response) {
                    self._lastSearchVal = _.escape(request.term);
                    self.call('knowledge_service', 'searchSections', self._lastSearchVal, 10).done(response);
                },
                select: function (ev, ui) {
                    var partnerID = ui.item.id;
                    var dmChat = self.call('knowledge_service', 'getDMChatFromSectionID', partnerID);
                    if (dmChat) {
                        self._setThread(dmChat.getID());
                    }
                    
                    $(this).val('');
                    return false;
                },
                focus: function (ev) {
                    ev.preventDefault();
                },
            });
        }
    },


    _setThread: function (threadID) {
        var self = this;
        this._thread = this.call('knowledge_service', 'getKmThread', threadID)
        return this._fetchAndRenderThread().then(function () {
            self.action_manager.do_push_state({
                action: self.action.id,
                active_id: self._thread.getID(),
            });
        });
    },

    _onSearch: function (ev) {
        ev.stopPropagation();
        var session = this.getSession();
        var result = pyUtils.eval_domains_and_contexts({
            domains: ev.data.domains,
            contexts: [session.user_context],
        });
        this.domain = result.domain;
        if (this._thread) {
            this._fetchAndRenderThread();
        }
    },

    _fetchAndRenderThread: function () {
        var self = this;
        return $.when().then(function () {
            self._threadWidget.render(
                self._thread,
                self._getKmThreadRenderingOptions()
            );
            self._updateButtonStatus();
        });
    },

    _updateButtonStatus: function (disabled, type) {
        if (this._thread.getID() === 'mailbox_articles') {
            this.trigger_up('show_effect', {
                message: _t("Congratulations, your inbox is empty!"),
                type: 'rainbow_man',
            });    
        }
    },

    _getKmThreadRenderingOptions: function () {
        return {
            displayMarkAsRead: this._thread.getID() === 'mailbox_articles',
        };
    },

    _renderSidebar: function (options) {

        var $sidebar = $(QWeb.render('knowledge.articles.Sidebar', {
            activeThreadID: this._thread ? this._thread.getID() : undefined,
            threads: options.threads,
        }));
        return $sidebar;
    },

});

core.action_registry.add('knowledge.articles', KnowledgeBase);

return KnowledgeBase;

});
