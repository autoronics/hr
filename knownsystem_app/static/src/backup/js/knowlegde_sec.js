odoo.define('knownsystem_app.Section', function (require) {
"use strict";

var SearchableThread = require('mail.model.SearchableThread');
var ThreadTypingMixin = require('mail.model.ThreadTypingMixin');
var mailUtils = require('mail.utils');

var session = require('web.session');
var time = require('web.time');

/**
 * This class represent channels in JS. In this context, the word channel
 * has the same meaning of channel on the server, meaning that direct messages
 * (DM) and livechats are also channels.
 *
 * Any piece of code in JS that make use of channels must ideally interact with
 * such objects, instead of direct data from the server.
 */
var Section = SearchableThread.extend(ThreadTypingMixin, {
	
});