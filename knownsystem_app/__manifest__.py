{
    "name" : "Knowledge Base System",
    "author": "Edge Technologies",
    "version" : "12.0.1.0",
    "live_test_url":"https://youtu.be/2NQ_p9AV-qA",
    "images":["static/description/main_screenshot.png"],
    "summary": "Knowledge Base System company FAQ company Policy updates knowledge management tool knowledge wiki program document knowledge document management knowledge portal for knowledge base portal knowsystems portal document portal Knowledge FAQ Knowledge Policy",
    "description": """
    
    Knowledge Base System is used for company internal FAQ and Policy updates
    
    """,
    "license" : "OPL-1",
    "depends" : ["base","web","portal","website"],
    "data": [
        "security/knownsystem_security.xml",
        "security/ir.model.access.csv",
        "views/knownsystem_template_view.xml",
        "views/knownsystem_view.xml",
        "views/knowsystem_base_view.xml",
        "views/knownsystem_template_portal.xml",
        "report/knowledge_bases_report.xml",
        "wizard/kms_mass_update_view.xml",
    ],
    "qweb": [
        "static/src/xml/kanban_extend_views.xml"
    ],

    'assets': {
        'web.assets_qweb': [
            '/knownsystem_app/static/src/xml/*.xml'
        ],

        'web.assets_frontend': [
            '/knownsystem_app/static/src/css/frontend_view.css',
            '/knownsystem_app/static/src/js/kmsfrontend.js',
        ],
        'web.assets_backend': [
            '/knownsystem_app/static/src/css/kms_kanban_view.css',
            '/knownsystem_app/static/src/css/jstree.min.css',
            '/knownsystem_app/static/src/js/fieldtexthtml.js',
            '/knownsystem_app/static/src/js/jstree.min.js',
            '/knownsystem_app/static/src/js/kanbanviewextended.js'
        ],
    },
    "auto_install": False,
    "installable": True,
    "price": 88,
    "currency": "EUR",
    "category" : "website",
}
