from odoo import api, fields, models, _
from odoo.exceptions import UserError
from lxml import etree

class KmsUpdate(models.TransientModel):
    _name = 'kms.mass.update'

    add_tag_ids = fields.Many2many('knowledge.tags','add_tag_kms_update',string='Add Tags')
    remove_tag_ids = fields.Many2many('knowledge.tags','remove_tag_kms_update',string='Remove Tags')
    section_id = fields.Many2one('knowledge.section',string='Section')
    website_publish = fields.Selection([
        ('publish','Publish'),
        ('unpublished','Unpublished')],string="Publish",default='publish')

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(KmsUpdate, self).fields_view_get(
            view_id=view_id,
            view_type=view_type,
            toolbar=toolbar, 
            submenu=submenu
        )
        active_model = self._context.get('active_model')
        active_ids = self._context.get('active_ids')
        orders = self.env[active_model].browse(active_ids)

        if view_type == 'form':
            doc = etree.XML(res['arch'])
            for node in doc.xpath("//field[@name='remove_tag_ids']"):
                result = []
                if orders:
                    data = []
                    for record in orders:
                        for tags in record.tag_ids:
                            data.append(tags.id)

                    node.set('domain', '[("id", "in", ' + str(list(set(data)))+')]')
                    res['arch'] = etree.tostring(doc)
            for node in doc.xpath("//field[@name='add_tag_ids']"):
                if orders:
                    remove_data = []
                    add_data = []
                    self.env.cr.execute("select id from knowledge_tags;")
                    extend_ids = self.env.cr.fetchall()
                    for record in orders:
                        for tags in record.tag_ids:
                            remove_data.append(tags.id)
                    
                    for extend_id in extend_ids:
                        add_data.append(int(extend_id[0]))

                    node.set('domain', '[("id", "in", ' + str(list(set(add_data) ^ set(remove_data)))+')]')
                    res['arch'] = etree.tostring(doc)
        return res


    @api.model
    def default_get(self, field):
        res = super(KmsUpdate, self).default_get(field)
        model = self._context.get('active_model')
        active_ids = self._context.get('active_ids')
        record_ids = self.env[model].browse(active_ids)
        data = []
        for record in record_ids:
            for tags in record.tag_ids:
                data.append(tags.id)
        res.update({
            'remove_tag_ids' : [(6,0,list(set(data)))]
            })
        return res


    def update_records(self):
        model = self._context.get('active_model')
        active_ids = self._context.get('active_ids')
        record_ids = self.env[model].browse(active_ids)
        add_tag = []
        remove_tag = []

        
        for remove in self.remove_tag_ids:
            remove_tag.append(remove.id)
        
        for add in self.add_tag_ids:
            add_tag.append(add.id)

        if self.website_publish == 'publish':
            website_publish = True
        else:
            website_publish = False


        for record in record_ids:
            for remove in remove_tag:
                record.write({'tag_ids' : [(3, remove)]})
            for add in add_tag:
                record.write({'tag_ids' : [(4, add)]})
            record.write({
                'section_id' : self.section_id.id or record.section_id.id or False,
                'website_published' : website_publish or record.website_published,
                })
            record.invalidate_cache()
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }
