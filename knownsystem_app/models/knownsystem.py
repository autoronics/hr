import json
from itertools import groupby
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang
from odoo.tools import html2plaintext
import odoo.addons.decimal_precision as dp
from odoo.tools import html2plaintext


class KnowSystemTags(models.Model):
    _name = 'knowledge.tags'
    _description = 'Knowledge Tags'

    name = fields.Char(string='Knowledge Tag')
    color = fields.Integer('Color Index')
    parent_id = fields.Many2one('knowledge.tags',string='Parent Tag')
    parent_lines = fields.One2many('knowledge.tags','parent_id',string='Parent Tags')

    def knowsystem_tags(self):
        tag_ids = self.env['knowledge.tags'].sudo().search([('parent_id','=',False)])
        data = []
        if tag_ids:
            for tag in tag_ids:
                parent_data = []
                records = {}
                if tag.parent_lines:
                    for parent in tag.parent_lines:
                        parent_rec = {}
                        parent_rec.update({
                            'id' : parent.id,
                            'text' : parent.name,
                        })
                        parent_data.append(parent_rec)
                    records.update({
                        'text' : tag.name,
                        'children' : parent_data
                        })  
                else:
                    records.update({
                        'id' : tag.id,
                        'text' : tag.name,
                    })
                data.append(records)
        return json.dumps(data)


class KnowledgeBaseSection(models.Model):
    _name = 'knowledge.section'
    _description = 'Knowledge Section'

    name = fields.Char(string='Knowledge Section')
    is_pinned = fields.Boolean(string='Is Pinned')
    parent_id = fields.Many2one('knowledge.section',string='Parent Section')
    parent_lines = fields.One2many('knowledge.section','parent_id',string='Parent Sections')


class KnowledgeBase(models.Model):
    _name = 'knowledge.bases'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _description = 'Knowledge Base'

    name = fields.Char(string='Article Name')
    section_id = fields.Many2one('knowledge.section',string="Knowledge Section")
    tag_ids = fields.Many2many('knowledge.tags',string="Knowledge Tags")
    article_text = fields.Html(string='Article Text')
    article_pain_text = fields.Text(string='Article Plain Text',compute='article_plain_text',store=True)
    like_count = fields.Integer('Liked',default=0, track_visibility='onchange',copy=False)
    dislike_count = fields.Integer('Disliked',default=0, track_visibility='onchange',copy=False)
    is_fav = fields.Boolean('Is Favourite')
    favourite_lines = fields.One2many('knw.favourite','knowledge_id')
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)
    partner_id = fields.Many2one('res.partner',string="Partner",related="user_id.partner_id",store=True,)
    like_lines = fields.One2many('like.dislike','knowledge_id')
    active = fields.Boolean(string='Active', default=True)
    website_published = fields.Boolean(string='Publish')
    website_url = fields.Char('Website URL', compute='_compute_website_url', help='The full URL to access the document through the website.')

    def _compute_website_url(self):
        for record in self:
            record.website_url = '#'


    def toggle_active(self):
        """ Inverse the value of the field ``active`` on the records in ``self``. """
        for record in self:
            record.active = not record.active


    def website_publish_button(self):
        if self._context.get('kanban'):
            for record in self:
                record.website_published = True
        else:
            self.ensure_one()
            if self.env.user.has_group('website.group_website_publisher') and self.website_url != '#':
                # Force website to land on record's website to publish/unpublish it
                if 'website_id' in self and self.env.user.has_group('website.group_multi_website'):
                    self.website_id._force()
                return self.open_website_url()
            return self.write({'website_published': not self.website_published})


    def open_website_url(self):
        return {
            'type': 'ir.actions.act_url',
            'url': self.website_url,
            'target': 'self',
        }

    def print_button(self):
        return self.env.ref('knownsystem_app.kms_report_id').report_action(self)


    def like_button(self):
        for base in self:
            count = 0
            user_id = self.env.user
            if len(base.like_lines) == 0:
                vals = {}
                vals.update({
                    'knowledge_id' : base.id,
                    'user_id' : user_id.id,
                    'status' : 'like',
                    })
                base.write({
                    'like_lines' : [(0,0,vals)]
                    })
                base.like_count += 1
            else:
                for like in base.like_lines:
                    if ((like.user_id.id == user_id.id) and (like.knowledge_id.id == base.id)):
                        if like.status == 'like':
                            if base.like_count >= 1:
                                base.like_count -= 1
                                like.write({'status' : 'notlike'})
                        elif like.status == 'notlike':
                                base.like_count += 1
                                like.write({'status' : 'like'})
                        elif like.status == 'dislike':
                            if base.dislike_count >= 1:
                                base.dislike_count -= 1
                            base.like_count += 1
                            like.write({'status' : 'like'})
                        elif like.status == 'notdislike':
                            base.like_count += 1
                            like.write({'status' : 'like'})
                    elif not any(like.user_id.id == user_id.id for like in base.like_lines):
                        if ((like.user_id.id != user_id.id) and (like.knowledge_id.id == base.id)):                        
                            base.write({'like_lines' : [(0,0,{'knowledge_id' : base.id,'user_id' : user_id.id,'status' : 'like',})]})
                            base.like_count += 1
                        else:
                            count += 1


    def dislike_recods(self, records):
        record_ids = self.env['knowledge.bases'].browse(int(records[0]))
        for _ in record_ids:
            _.dislike_button()
        return True


    def like_recods(self, records):
        record_ids = self.env['knowledge.bases'].browse(int(records[0]))
        for _ in record_ids:
            _.like_button()
        return True


    def publish_recods(self, records):
        record_ids = self.env['knowledge.bases'].browse(records)
        for _ in record_ids:
            _.with_context(kanban=True).website_publish_button()
        return True


    def archive_recods(self, records):
        record_ids = self.env['knowledge.bases'].browse(records)
        for _ in record_ids:
            _.toggle_active()
        return True


    def duplicate_recods(self, records):
        record_ids = self.env['knowledge.bases'].browse(records)
        for _ in record_ids:
            _.sudo().copy()
        return True


    def dislike_button(self):
        for base in self:
            user_id = self.env.user
            if len(base.like_lines) == 0:
                vals = {}
                vals.update({
                    'knowledge_id' : base.id,
                    'user_id' : user_id.id,
                    'status' : 'dislike',
                    })
                base.write({
                    'like_lines' : [(0,0,vals)]
                    })
                base.dislike_count += 1
            else:
                for like in base.like_lines:
                    if ((like.user_id.id == user_id.id) and (like.knowledge_id.id == base.id)):
                        if like.status == 'like':
                            if base.like_count >= 1:
                                base.like_count -= 1
                            base.dislike_count += 1
                            like.write({'status' : 'dislike'})
                        elif like.status == 'dislike':
                            if base.dislike_count >= 1:
                                base.dislike_count -= 1
                            like.write({'status' : 'notdislike'})
                        elif like.status == 'notdislike':
                            base.dislike_count += 1
                            like.write({'status' : 'dislike'})
                        elif like.status == 'notlike':
                            base.dislike_count += 1
                            like.write({'status' : 'dislike'})
                    elif not any(like.user_id.id == user_id.id for like in base.like_lines):
                        if ((like.user_id.id != user_id.id) and (like.knowledge_id.id == base.id)):                        
                            base.write({'like_lines' : [(0,0,{'knowledge_id' : base.id,'user_id' : user_id.id,'status' : 'dislike',})]})
                            base.dislike_count += 1
                        else:
                            count += 1


    @api.depends('article_text')
    def article_plain_text(self):
        for base in self:
            if base.article_text:
                base.update({'article_pain_text' : html2plaintext(base.article_text)})


class KnowledgeBaseFav(models.Model):
    _name = 'knw.favourite'
    _description = 'Favourite'

    knowledge_id = fields.Many2one('knowledge.bases')
    user_id = fields.Many2one('res.users')
    is_fav = fields.Boolean('Is favorite')


class KnowledgeBaseLikeDisLike(models.Model):
    _name = 'like.dislike'
    _description = 'Like DisLike'

    knowledge_id = fields.Many2one('knowledge.bases')
    user_id = fields.Many2one('res.users')
    status = fields.Selection([
        ('like','Like'),
        ('dislike','DisLike'),
        ('notlike','Remove Like'),
        ('notdislike','Remove DisLike'),
        ],string="Status")


class MailComposeMathodIn(models.TransientModel):
    _inherit = 'mail.compose.message'

    use_article = fields.Boolean(string='Use Article',default=False)
    article_id = fields.Many2one('knowledge.bases')

    @api.model
    def default_get(self,fields):
        res = super(MailComposeMathodIn, self).default_get(fields)
        config_obj = self.env['res.config.settings'].sudo().search([], limit=1, order="id desc")
        res.update({
            'use_article' : config_obj.use_article
            })
        return res

    @api.onchange('article_id')
    def onchange_article(self):
        for mail in self:
            if mail.article_id:
                mail.body = mail.article_id.article_text

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    use_article = fields.Boolean(string="Use Article In Mail")


    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        config_parameter = self.env['ir.config_parameter'].sudo()
        use_article = config_parameter.get_param('knownsystem_app.use_article')
        res.update(use_article=use_article)
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        config_parameter = self.env['ir.config_parameter'].sudo()
        config_parameter.set_param('knownsystem_app.use_article',self.use_article)
