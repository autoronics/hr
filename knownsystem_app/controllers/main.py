import base64
import logging
import psycopg2
import werkzeug
import json
from werkzeug import url_encode
from collections import OrderedDict
from odoo import api, http, registry, SUPERUSER_ID, _
# from odoo.addons.web.controllers.main import binary_content
from odoo.exceptions import AccessError
from odoo.http import request
from odoo.tools import consteq, pycompat
from odoo.addons.web_editor.controllers.main import Web_Editor
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager, get_records_pager
from odoo.osv.expression import OR

_logger = logging.getLogger(__name__)

class Web_Editors(Web_Editor):

    @http.route('/knownsystem_app/knowledge_template', type='http', auth="user")
    def kms_system_FieldTextHtmlEmailTemplate(self, model=None, res_id=None, field=None, callback=None, **kwargs):
        kwargs['snippets'] = '/knownsystem_app/snippets'
        kwargs['template'] = 'knownsystem_app.FieldTextHtml'
        return self.FieldTextHtml(model, res_id, field, callback, **kwargs)

    @http.route(['/knownsystem_app/snippets'], type='json', auth="user", website=True)
    def kms_system_snippets(self):
        values = {'company_id': request.env['res.users'].browse(request.uid).company_id}
        return request.env.ref('knownsystem_app.kms_snippets').render(values)


class KmsSystems(http.Controller):

    @http.route(['/kms/sections'], type='http', auth="public", website=True)
    def kms_sections(self, **post):
        sections_ids = request.env['knowledge.section'].sudo().search([],order='id desc')

        nd_sections_ids = request.env['knowledge.section'].sudo().search([],order='id asc')

        data = []

        if nd_sections_ids:
            for section in nd_sections_ids:
                parent_data = []
                records = {}
                if not section.parent_id:
                    if section.parent_lines:
                        for parent in section.parent_lines:
                            parent_rec = {}
                            parent_rec.update({
                                'id' : parent.id,
                                'text' : parent.name,
                            })
                            parent_data.append(parent_rec)
                        records.update({
                            'id' : section.id,
                            'text' : section.name,
                            'children' : parent_data
                        })
                        data.append(records)
                    else:
                        records.update({
                            'id' : section.id,
                            'text' : section.name,
                        })
                        data.append(records) 
                elif section.parent_id and section.parent_lines:
                    for _ in data:
                        for __ in _['children']:
                            if __['id'] == section.id:
                                parent_data = []
                                for parent in section.parent_lines:
                                    parent_rec = {}
                                    parent_rec.update({
                                        'id' : parent.id,
                                        'text' : parent.name,
                                    })
                                    parent_data.append(parent_rec)
                                __.update({
                                    'children' : parent_data
                                })
        return json.dumps(data)

    @http.route(['/kms/tags'], type='http', auth="public", website=True)
    def kms_tags(self, **post):
        tag_ids = request.env['knowledge.tags'].sudo().search([],order='id asc')
        data = []
        if tag_ids:
            for tag in tag_ids:
                parent_data = []
                records = {}
                if not tag.parent_id:
                    if tag.parent_lines:
                        for parent in tag.parent_lines:
                            parent_rec = {}
                            parent_rec.update({
                                'id' : parent.id,
                                'text' : parent.name,
                            })
                            parent_data.append(parent_rec)
                        records.update({
                            'id' : tag.id,
                            'text' : tag.name,
                            'children' : parent_data
                        })
                        data.append(records)
                    else:
                        records.update({
                            'id' : tag.id,
                            'text' : tag.name,
                        })
                        data.append(records) 
                elif tag.parent_id and tag.parent_lines:
                    for _ in data:
                        for __ in _['children']:
                            if __['id'] == tag.id:
                                parent_data = []
                                for parent in tag.parent_lines:
                                    parent_rec = {}
                                    parent_rec.update({
                                        'id' : parent.id,
                                        'text' : parent.name,
                                    })
                                    parent_data.append(parent_rec)
                                __.update({
                                    'children' : parent_data
                                })                
        return json.dumps(data)


    @http.route(['/kms/articles'], type='http', auth="public", website=True)
    def kms_articles(self, **post):
        
        data = []
        all_records_name = []
        final_list = []
        if post.get('articles'):
            try_list = post.get('articles').strip('][').split(',')
            
            for item in try_list:
                final_list.append(int(item))
            
            record_ids = request.env['knowledge.bases'].search([('id','in',final_list)])

            for rec in record_ids:
                all_records_name.append(rec.name)

            record = {
                'articles' : record_ids.ids,
                'names' : all_records_name,
                'counts' : len(record_ids.ids)
            }
            data.append(record)
            
        return json.dumps(data)

    

class CustomerPortal(CustomerPortal):
    @http.route(['/my/kms', '/my/kms/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_kms_articles(self, page=1, date_begin=None, date_end=None, sortby=None, filterby=None, search=None, search_in='content', **kw):
        values = self._prepare_portal_layout_values()

        searchbar_sortings = {
            'article_pain_text': {'label': _('Text'), 'order': 'article_pain_text'},
            'name': {'label': _('Name'), 'order': 'name'},
        }
        searchbar_filters = {
            'all': {'label': _('All'), 'domain': []},
        }
        searchbar_inputs = {
            'article_pain_text': {'input': 'article_pain_text', 'label': _('Search (in Content)')},
            'name': {'input': 'name', 'label': _('Search in Name')},
        }

        domain = ([('website_published','=',True)])

        if not sortby:
            sortby = 'name'
        order = searchbar_sortings[sortby]['order']
        # default filter by value
        if not filterby:
            filterby = 'all'
        # domain = searchbar_filters[filterby]['domain']
        partner = request.env.user.partner_id
        kms_obj = http.request.env['knowledge.bases']

        # search
        if search and search_in:
            search_domain = []
            if search_in in ('article_pain_text', 'all'):
                search_domain = OR([search_domain, [('article_pain_text', 'ilike', search)]])
            if search_in in ('name', 'all'):
                search_domain = OR([search_domain, [('name', 'ilike', search)]])
            domain += search_domain


        kms_count = kms_obj.sudo().search_count(domain)
        # pager
        pager = request.website.pager(
            url="/my/kms",
            total=kms_count,
            page=page,
            step=self._items_per_page
        )
        # content according to pager and archive selected
        kms_articles = kms_obj.sudo().search(domain, order=order, limit=self._items_per_page, offset=pager['offset'])
        values.update({
            'kms_articles': kms_articles,
            'page_name': 'kms',
            'pager': pager,
            'default_url': '/my/kms',
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
            'searchbar_filters': OrderedDict(sorted(searchbar_filters.items())),
            'filterby': filterby,
        })
        return request.render("knownsystem_app.display_kms_articles", values)


    @http.route(['/my/kms/<int:kms_id>'], type='http', auth="user", website=True)
    def my_kms_article(self, kms_id=None, **kw):
        kms = request.env['knowledge.bases'].browse(kms_id)
        return request.render("knownsystem_app.my_kms_article", {'kms': kms})