# Company: giordano.ch AG
# Copyright by: giordano.ch AG
# https://giordano.ch/

{
    'name': 'Stop Auto Followers',
    'version': '15.0.0.0',
    'author': 'giordano.ch AG',
    'website': 'https://www.giordano.ch',
    'license': 'OPL-1',
    'category': 'mail',
    'sequence': '10',
    'summary': 'Stop Auto Followers Partners',
    'description':  """This Module will Stop Auto Followers Partners""",
    'depends': ['mail'],
    'data': ['wizard/mail_compose_message_views.xml'],
    'images': ['static/description/logo.jpg'],
    'application': True,
    'price': 10,
    'currency': 'EUR',
}
