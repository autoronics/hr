# Company: giordano.ch AG
# Copyright by: giordano.ch AG
# https://giordano.ch/
{
    'name': 'Website Multiple Menus/Submenus',
    'category': 'Website',
    'summary': 'Website Multiple Menus/Submenus',

    'version': '15.0.1',
    'description': """
Website Multiple Menus/Submenus
===============================
This module allows to list multiple menus/submenus in website.
        """,

    'author': 'giordano.ch AG',
    'support': 'support@giordano.ch',
    'website': 'https://www.giordano.ch',
    'depends': [
        'website'
    ],
    'assets': {
        'web.assets_frontend':[
            'website_multi_menu_submenu_oin/static/src/js/menu.js',
            'website_multi_menu_submenu_oin/static/src/css/menu.css',
        ],
    },

    'data': [
        'views/assets.xml',
        'views/website_templates.xml',
    ],
    'images': ['images/OdooITnow_screenshot.png'],

    'price': 10.0,
    'currency': 'EUR',

    'installable': True,
    'application': False
}
