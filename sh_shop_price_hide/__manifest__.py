# Company: giordano.ch AG
# Copyright by: giordano.ch AG
# https://giordano.ch/

{
    'name': 'Product Price Hide in Shop',
    'summary': """invisible product price module, hide product price app, remove product price in shop, 
                  temporary delete product price, hide shop product price odoo""",
    'description': """ This module useful to hide product price in shop. 
                       Only logged user can see product price.invisible product price module, hide product price app,
                       remove product price in shop, temporary delete product price, hide shop product price odoo """,
    'version': '15.0.1.0.2',
    'category': 'eCommerce',
    'license': 'OPL-1',
    'author': 'giordano.ch AG',
    'support': 'support@giordano.ch',
    'website': 'https://www.giordano.ch',
    'depends': ['base', 'website_sale'],
    'data': ['views/website_sale_template.xml'],
    'images': ['static/description/background.png'],
    'auto_install': False,
    'application': True,
    'installable': True,
    'price': 8,
    'currency': "EUR"
}
